//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/05/28                       
 * 
 * Description:
 * - Testing of the vpl::CStaticVector template.
 */

#include <VPL/Math/StaticVector.h>

// STL
#include <iostream>


//==============================================================================
/*!
 * Prints a given vector
 */
void printVector(vpl::math::CDVector3& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    vpl::math::CDVector4 v4(0, 1, 2, 3);
    vpl::math::CDVector3 v1;
    v1.fill(0);
    std::cout << "Vector 1" << std::endl;
    printVector(v1);
    keypress();

    vpl::math::CDVector3 v2;
    std::cout << "Vector 2" << std::endl;
    for( vpl::tSize j = 0; j < v2.getSize(); j++ )
    {
        v2(j) = j;
    }
    printVector(v2);
    keypress();

    std::cout << "Operation v1 += v2" << std::endl;
    v1 += v2;
    printVector(v1);
    keypress();

    std::cout << "Operation v1 *= 2" << std::endl;
    v1 *= vpl::CScalar<int>(2);
    printVector(v1);
    keypress();

    std::cout << "Operation v2 += 10" << std::endl;
    v2 += vpl::CScalar<int>(10);
    printVector(v2);
    keypress();

    std::cout << "Operation v1.zeros()" << std::endl;
    v1.zeros();
    printVector(v1);
    keypress();

    std::cout << "Operation v2.ones()" << std::endl;
    v2.ones();
    printVector(v2);
    keypress();

    return 0;
}

