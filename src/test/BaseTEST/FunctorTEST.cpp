//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2008 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2008/02/25                       
 *
 * Description:
 * - Testing of the vpl::CFunctor template.
 */

#include <VPL/Base/Functor.h>

// STL
#include <string>
#include <iostream>


//==============================================================================
/*!
 * Test function.
 */
double getMax(double a, double b)
{
    std::cout << "double getMax(" << a << ", " << b << ")" << std::endl;
    return (a > b) ? a : b;
}


//==============================================================================
/*!
 * Test object.
 */
class CTest
{
public:
    //! Default constructor
    CTest() {}

    //! Destructor
    virtual ~CTest() {}

    //! Method
    virtual void print1(int a)
    {
        std::cout << "void CTest::print1(" << a << ")" << std::endl;
    }

    //! Method
    double getMax(double a, double b)
    {
        std::cout << "double CTest::getMax(" << a << ", " << b << ")" << std::endl;
        return (a > b) ? a : b;
    }

    //! Static method
    static void print2(int a, bool b)
    {
        std::cout << "void CTest::print2(" << a << ", " << b << ")" << std::endl;
    }
};


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    vpl::base::CFunctor<double, double, double> Max(getMax);
    double dMax = Max(5, 10.0);
    std::cout << "  Result = " << dMax << std::endl;

    vpl::base::CFunctor<void, int, bool> Print2(CTest::print2);
    Print2(10, false);

    CTest Test;

    Max = vpl::base::CFunctor<double, double, double>(&Test, &CTest::getMax);
    dMax = Max(15.0f, 10);
    std::cout << "  Result = " << dMax << std::endl;

    vpl::base::CFunctor<void, int> Print1(&Test, &CTest::print1);
    Print1(1000);
    keypress();

    return 0;
}
