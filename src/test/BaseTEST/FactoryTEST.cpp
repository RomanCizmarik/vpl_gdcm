//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2005/03/04                       
 *
 * Description:
 * - Testing of the vpl::CFactory template.
 */

#include <VPL/Base/Factory.h>

// STL
#include <string>
#include <iostream>


//==============================================================================
/*!
 * Object CA
 */
class CA
{
public:
    //! Default constructor
    CA() { std::cout << "  CA::CA()" << std::endl; }

    //! Destructor
    virtual ~CA() { std::cout << "  CA::~CA()" << std::endl; }

    //! Virtual method
    virtual void print() { std::cout << "  CA::print()" << std::endl; }

};


//==============================================================================
/*!
 * Object CAA
 */
class CAA : public CA
{
public:
    //! Destructor
    ~CAA() { std::cout << "  CAA::~CAA()" << std::endl; }

    //! Virtual method
    void print() { std::cout << "  CAA::print()" << std::endl; }

public:
    //! Object cretation function
    static CA *create() { return new CAA; }

private:
    //! Default constructor
    CAA() { std::cout << "  CAA::CAA()" << std::endl; }

};


//==============================================================================
/*!
 * Object CAB
 */
class CAB : public CA
{
public:
    //! Destructor
    ~CAB() { std::cout << "  CAB::~CAB()" << std::endl; }

    //! Virtual method
    void print() { std::cout << "  CAB::print()" << std::endl; }

public:
    //! Object cretation function
    static CA *create() { return new CAB; }

private:
    //! Default constructor
    CAB() { std::cout << "  CAB::CAB()" << std::endl; }

};


//==============================================================================
/*!
 * Object factory
 */
typedef vpl::base::CFactory<CA, std::string> tFactory;


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    std::cout << "Register all object creation functions" << std::endl;
    std::cout << "CAA: " << VPL_FACTORY(tFactory).registerObject("CAA", CAA::create) << std::endl;
    std::cout << "CAB: " << VPL_FACTORY(tFactory).registerObject("CAB", CAB::create) << std::endl;
    keypress();

    std::cout << "Create object CAB" << std::endl;
    CA *pCA = VPL_FACTORY(tFactory).create("CAB");
    pCA->print();
    keypress();

    std::cout << "Delete object" << std::endl;
    delete pCA;
    keypress();

    std::cout << "Unregister CAB object creation function" << std::endl;
    std::cout << "CAB: " << VPL_FACTORY(tFactory).unregisterObject("CAB") << std::endl;
    keypress();

    std::cout << "Create object CAA" << std::endl;
    pCA = VPL_FACTORY(tFactory).create("CAA");
    pCA->print();
    keypress();

    std::cout << "Delete object" << std::endl;
    delete pCA;
    keypress();

    std::cout << "Unregister CAA object creation function" << std::endl;
    std::cout << "  CAA: " << VPL_FACTORY(tFactory).unregisterObject("CAA") << std::endl;
    keypress();

    try
    {
        std::cout << "Create object CAA" << std::endl;
        pCA = VPL_FACTORY(tFactory).create("CAA");
        pCA->print();
        keypress();
    }
    catch( vpl::base::Factory::CCannotCreate& Exception )
    {
        std::cout << Exception.what() << std::endl;
    }

    std::cout << "Exit" << std::endl;

    return 0;
}

