#==============================================================================
# This file is part of
#
# VPL - Voxel Processing Library
# Changes are Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#==============================================================================

SET( VPL_TESTS LogTEST )

FOREACH( VPL_TEST ${VPL_TESTS} )

  ADD_EXECUTABLE( ${VPL_TEST} ${VPL_TEST}.cpp )
  TARGET_LINK_LIBRARIES( ${VPL_TEST}
                         ${VPL_LIBS}
                         ${VPL_SYSTEM_LIBS} )
  SET_TARGET_PROPERTIES( ${VPL_TEST} PROPERTIES
                         DEBUG_POSTFIX d
                         LINK_FLAGS "${VPL_LINK_FLAGS}" )

ENDFOREACH( VPL_TEST )
