//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2008 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2008/02/25                       
 *
 * Description:
 * - Testing of the vpl::mod::CSignal class template.
 */

#include <VPL/Base/Setup.h>
#include <VPL/Module/Signal.h>

// STL
#include <string>
#include <iostream>


//==============================================================================
/*!
 * Test function.
 */
double getMax(double a, double b)
{
    std::cout << "  double getMax(" << a << ", " << b << ")" << std::endl;
    return (a > b) ? a : b;
}

void print1(int a)
{
    std::cout << "  void print1(" << a << ")" << std::endl;
}

void printHello()
{
    std::cout << "  Hello ";
}


//==============================================================================
/*!
 * Test object.
 */
class CTest
{
public:
    //! Default constructor
    CTest() {}

    //! Destructor
    virtual ~CTest() {}

    //! Method
    virtual void print1(int a)
    {
        std::cout << "  void CTest::print1(" << a << ")" << std::endl;
    }

    //! Method
    void printWorld()
    {
        std::cout << "world!" << std::endl;
    }
};


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    std::cout << "Create a new signal Sig0" << std::endl;
    vpl::mod::CSignal<double, double, double> Sig0;

    std::cout << "  Register a new handler (getMax() function)" << std::endl;
    vpl::mod::tSignalConnection Con0 = Sig0.connect(getMax);

    std::cout << "  Invoke the signal" << std::endl;
    double dMax = Sig0.invoke2(10.0, 5);
    std::cout << "  Result = " << dMax << std::endl;

    std::cout << "  Deregister the getMax() function and invoke" << std::endl;
    Sig0.disconnect(Con0);
//    Sig0.block(Con0);
    dMax = Sig0.invoke2(15.0f, 10);
    std::cout << "  Result = " << dMax << std::endl;
    keypress();


    CTest Test;

    std::cout << "Create a new signal Sig1" << std::endl;
    vpl::mod::CSignal<void> Sig1;

    std::cout << "  Register a new handlers (printHello() and CTest::printWorld() functions)" << std::endl;
    vpl::mod::tSignalConnection Con10 = Sig1.connect(printHello);
    vpl::mod::tSignalConnection Con11 = Sig1.connect(&Test, &CTest::printWorld);

    std::cout << "  Invoke the signal" << std::endl;
    Sig1.invoke();

    std::cout << "  Block the CTest::printWorld() function and invoke" << std::endl;
    std::cout << "  ";
    Sig1.block(Con11);
    Sig1.invoke();
    keypress();

    std::cout << "Create a new signal Sig2" << std::endl;
    vpl::mod::CSignal<void, int> Sig2;

    std::cout << "  Register a new handlers (print1() and CTest::print1() functions)" << std::endl;
    vpl::mod::tSignalConnection Con20 = Sig2.connect(print1);
    vpl::mod::tSignalConnection Con21 = Sig2.connect(&Test, &CTest::print1);

    std::cout << "  Invoke the signal" << std::endl;
    Sig2.invoke(20);

    std::cout << "  Deregister both handlers and invoke the signal" << std::endl;
    Sig2.disconnect(Con20);
    Sig2.disconnect(Con21);
    Sig2.invoke(200);
    keypress();

    return 0;
}
