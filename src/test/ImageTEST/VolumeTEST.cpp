//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/06/01                       
 *
 * Description:
 * - Testing of the vpl::CVolume template.
 */

#include <VPL/Base/PartedData.h>
#include <VPL/Image/Image.h>
#include <VPL/Image/Volume.h>
#include <VPL/Image/VolumeFunctions.h>
#include <VPL/Module/Serialization.h>

// STL
#include <iostream>
#include <string>
#include <ctime>

// Default allocator
//typedef vpl::img::CVolume<vpl::img::tDensityPixel> tVolume;

// Parted allocator
typedef vpl::img::CVolume<vpl::img::tDensityPixel, vpl::base::CPartedData> tVolume;

typedef tVolume::tSmartPtr tVolumePtr;


//==============================================================================
/*!
 * Prints a given image
 */
void printImage(vpl::img::CDImage& Image, bool bPrintMargin = false)
{
    std::cout.setf(std::ios_base::fixed);
    vpl::tSize Margin = (bPrintMargin) ? Image.getMargin() : 0;
    for( vpl::tSize j = -Margin; j < Image.getYSize() + Margin; j++ )
    {
        std::cout << "  ";
        for( vpl::tSize i = -Margin; i < Image.getXSize() + Margin; i++ )
        {
            std::cout << Image(i, j) << "  ";
        }
        std::cout << std::endl;
    }
}


//==============================================================================
/*!
 * Prints a given volume
 */
void printVolume(tVolume& Volume, bool bPrintMargin = false)
{
    static char pcPrefix[255];

    std::cout.setf(std::ios_base::fixed);
    vpl::tSize Margin = (bPrintMargin) ? Volume.getMargin() : 0;
    for( vpl::tSize j = -Margin; j < Volume.getYSize() + Margin; j++ )
    {
        for( vpl::tSize k = -Margin; k < Volume.getZSize() + Margin; k++ )
        {
            memset(pcPrefix, (int)' ', Volume.getZSize() - k + 1);
            pcPrefix[Volume.getZSize() - k + 1] = '\0';
            std::cout << pcPrefix;
            for( vpl::tSize i = -Margin; i < Volume.getXSize() + Margin; i++ )
            {
                std::cout << Volume(i, j, k) << "  ";
            }
            std::cout << std::endl << std::endl;
        }
    }
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * Clock counter
 */
clock_t ClockCounter;


//==============================================================================
/*!
 * Starts time measuring
 */
void begin()
{
    ClockCounter = clock();
}

//==============================================================================
/*!
 * Stops time measuring and prints result
 */
void end()
{
    ClockCounter = clock() - ClockCounter;
    std::cout << "  Measured clock ticks: " << ClockCounter << std::endl;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    tVolumePtr spVolume1(new tVolume(3, 3, 3, 1));
    spVolume1->fillEntire(tVolume::tVoxel(123));
    std::cout << "Smart pointer to volume 1" << std::endl;
    printVolume(*spVolume1);
    keypress();

    tVolume Volume2(*spVolume1, 1, 1, 1, 2, 2, 2, vpl::REFERENCE);
    std::cout << "Creating volume 2 using constructor parameters (*spVolume1, 1, 1, 1, 2, 2, 2, vpl::REFERENCE)" << std::endl;
    std::cout << "- Reference to the volume 1" << std::endl;
    vpl::tSize i, j, k;
    for( k = 0; k < Volume2.getZSize(); k++ )
    {
        for( j = 0; j < Volume2.getYSize(); j++ )
        {
            for( i = 0; i < Volume2.getXSize(); i++ )
            {
                Volume2(i, j, k) = k * Volume2.getYSize() * Volume2.getZSize() + j * Volume2.getXSize() + i;
            }
        }
    }
    printVolume(Volume2);
    keypress();

    std::cout << "Volume 1" << std::endl;
    printVolume(*spVolume1);
    keypress();

    std::cout << "Volume slice - plane XY, z = 1" << std::endl;
    vpl::img::CDImage Plane(3, 3, 2);
    Plane.fillEntire(vpl::img::CDImage::tPixel(0));
    spVolume1->getPlaneXY(1, Plane);
    printImage(Plane);
    keypress();

    std::cout << "Volume slice - plane XZ, y = 2" << std::endl;
    spVolume1->getPlaneXZ(2, Plane);
    printImage(Plane);
    keypress();

    std::cout << "Operation *spVolume1 += Volume2" << std::endl;
    *spVolume1 += Volume2;
    printVolume(*spVolume1);
    keypress();

    std::cout << "Operation Volume2 *= 2" << std::endl;
    Volume2 *= vpl::CScalar<int>(2);
    printVolume(Volume2);
    keypress();

    std::cout << "Volume 1" << std::endl;
    printVolume(*spVolume1);
    keypress();

    std::cout << "Operation vpl::img::getMin<double>(*spVolume1), getMax(), getSum(), getMean()" << std::endl;
    std::cout << "  " << vpl::img::getMin<double>(*spVolume1)
    << "  " << vpl::img::getMax<double>(*spVolume1)
    << "  " << vpl::img::getSum<double>(*spVolume1)
    << "  " << vpl::img::getMean<double>(*spVolume1)
    << std::endl;
    keypress();

    tVolume Volume3(*spVolume1, 0, 0, 0, 2, 2, 2);
    std::cout << "Creating volume 3 using constructor parameters (*spVolume1, 0, 0, 0, 2, 2, 2)" << std::endl;
    std::cout << "- Copy of the volume 1" << std::endl;
    printVolume(Volume3);
    keypress();

    std::cout << "Volume 3" << std::endl;
    printVolume(Volume3);
    keypress();

    std::cout << "Operation Volume3 /= 2" << std::endl;
    Volume3 /= vpl::CScalar<int>(2);
    printVolume(Volume3);
    keypress();

    std::cout << "Volume 1" << std::endl;
    printVolume(*spVolume1);
    keypress();

    Volume3.resize(2, 2, 2, 1);
    Volume3.fillEntire(tVolume::tVoxel(0));
    std::cout << "Creating volume 3 with margin (3, 3, 3, tVoxel(0), 1)" << std::endl;
    for( k = 0; k < Volume3.getZSize(); k++ )
    {
        for( j = 0; j < Volume3.getYSize(); j++ )
        {
            for( i = 0; i < Volume3.getXSize(); i++ )
            {
                Volume3(i, j, k) = k * Volume3.getYSize() * Volume3.getZSize() + j * Volume3.getXSize() + i;
            }
        }
    }
    printVolume(Volume3, true);
    keypress();

    std::cout << "Fill margin of the volume 3" << std::endl;
    Volume3.fillMargin(1);
    printVolume(Volume3, true);
    keypress();

    std::cout << "Mirror margin of the volume 3" << std::endl;
    Volume3.mirrorMargin();
    printVolume(Volume3, true);
    keypress();

    std::cout << "Volume 2" << std::endl;
    printVolume(Volume2);
    keypress();

    std::cout << "Iterate volume 2" << std::endl;
    std::cout << "  ";
    {
        tVolume::tIterator it(Volume2);
        std::cout << "it.getSize(): " << it.getSize() << std::endl;
        for( ; it; ++it )
        {
            std::cout << *it << "  ";
        }
    }
    std::cout << std::endl;
    keypress();


    std::cout << "Testing voxel access overload" << std::endl;
    Volume3.resize(256, 256, 256, 8);

#ifdef _DEBUG
    int c, COUNT = 5;
#else
    int c, COUNT = 100;
#endif

    std::cout << "  Basic version" << std::endl;
    begin();
    for (c = 0; c < COUNT; ++c)
    {
        for (k = 0; k < Volume3.getZSize(); ++k)
        {
            for (j = 0; j < Volume3.getYSize(); ++j)
            {
                for (i = 0; i < Volume3.getXSize(); ++i)
                {
                    Volume3(i, j, k) = 1234;
                }
            }
        }
    }
    end();
    keypress();

    std::cout << "  Iterator version" << std::endl;
    begin();
    for (c = 0; c < COUNT; ++c)
    {
        ;
        for (tVolume::tIterator it(Volume3); it; ++it)
        {
            *it = 1234;
        }
    }
    end();
    keypress();

    std::cout << "  Fill version" << std::endl;
    begin();
    for (c = 0; c < COUNT; ++c)
    {
        Volume3.fill(1234);
    }
    end();
    keypress();

    std::cout << "  Fill entire version" << std::endl;
    begin();
    for (c = 0; c < COUNT; ++c)
    {
        Volume3.fillEntire(1234);
    }
    end();
    keypress();


    std::cout << "Testing dummy volumes" << std::endl;
    Volume3.enableDummyMode(true);
    Volume3.resize(3, 3, 3, 1);
    for (k = 0; k < Volume3.getZSize(); k++)
    {
        for (j = 0; j < Volume3.getYSize(); j++)
        {
            for (i = 0; i < Volume3.getXSize(); i++)
            {
                Volume3(i, j, k) = k * Volume3.getYSize() * Volume3.getZSize() + j * Volume3.getXSize() + i;
            }
        }
    }
    printVolume(Volume3, true);
    keypress();

    std::cout << "Fill margin of the dummy volume 3" << std::endl;
    Volume3.fillMargin(1);
    printVolume(Volume3, true);
    keypress();

    std::cout << "Serialize the dummy volume into a binary file" << std::endl;
    vpl::mod::save(Volume3, "dummy.vlm");
    keypress();

    std::cout << "Testing voxel access overload for dummy volumes" << std::endl;
    Volume3.resize(256, 256, 256, 8);

    std::cout << "  Basic version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        for( k = 0; k < Volume3.getZSize(); ++k )
        {
            for( j = 0; j < Volume3.getYSize(); ++j )
            {
                for( i = 0; i < Volume3.getXSize(); ++i )
                {
                    Volume3(i, j, k) = 1234;
                }
            }
        }
    }
    end();
    keypress();

    std::cout << "  Iterator version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        ;
        for( tVolume::tIterator it(Volume3); it; ++it )
        {
            *it = 1234;
        }
    }
    end();
    keypress();

    std::cout << "  Fill version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        Volume3.fill(1234);
    }
    end();
    keypress();

    std::cout << "  Fill entire version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        Volume3.fillEntire(1234);
    }
    end();
    keypress();

    std::cout << "Switch the volume back to the regular mode" << std::endl;
    Volume3.enableDummyMode(false);
    Volume3.resize(3, 3, 3, 1);
    Volume3.fillEntire(0);
    for (k = 0; k < Volume3.getZSize(); k++)
    {
        for (j = 0; j < Volume3.getYSize(); j++)
        {
            for (i = 0; i < Volume3.getXSize(); i++)
            {
                Volume3(i, j, k) = k * Volume3.getYSize() * Volume3.getZSize() + j * Volume3.getXSize() + i;
            }
        }
    }
    printVolume(Volume3, true);
    keypress();

    std::cout << "Fill margin of the volume 3" << std::endl;
    Volume3.fillMargin(1);
    printVolume(Volume3, true);
    keypress();

    std::cout << "Load the dummy volume back from the file" << std::endl;
    vpl::mod::load(Volume3, "dummy.vlm");
    printVolume(Volume3, true);

    return 0;
}

