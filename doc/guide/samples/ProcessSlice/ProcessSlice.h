//==============================================================================
/*
 * This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2010 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/10/31                          \n
 *
 * File description:
 * - Sample slice processing module.
 */

#ifndef MDS_PROCESS_SLICE_H
#define MDS_PROCESS_SLICE_H

#include <MDSTk/Module/mdsModule.h>


//==============================================================================
/*!
 * Sample module.
 */
class CProcessSlice : public mds::mod::CModule
{
public:
    //! Smart pointer type.
    //! - Declares type tSmartPtr.
    MDS_SHAREDPTR(CProcessSlice);

public:
    //! Default constructor.
    CProcessSlice(const std::string& sDescription);

    //! Virtual destructor.
    virtual ~CProcessSlice();

protected:
    //! Virtual method called on startup.
    virtual bool startup();

    //! Virtual method called by the processing thread.
    virtual bool main();

    //! Called on console shutdown.
    virtual void shutdown();

    //! Called on writing an usage statement.
    virtual void writeExtendedUsage(std::ostream& Stream);

protected:
    //! Command line arguments.
//    int m_iA;
//    bool m_bB;
};


//==============================================================================
/*!
 * Smart pointer to console application.
 */
typedef CProcessSlice::tSmartPtr    CProcessSlicePtr;


#endif // MDS_PROCESS_SLICE_H

