================================================================================
- This file comes from MDSTk software and was modified for
-
- VPL - Voxel Processing Library
- Changes are Copyright 2014 3Dim Laboratory s.r.o.
- All rights reserved.
-
- Use of this source code is governed by a BSD-style license that can be
- found in the LICENSE file.
-
- The original MDSTk legal notice can be found below.
-
- Medical Data Segmentation Toolkit (MDSTk)
- Copyright (c) 2003-2010 by Michal Spanel, FIT, Brno University of Technology
-
- Authors: Michal Spanel (spanel@fit.vutbr.cz)
- Date:    2010/08/20
================================================================================

This is a package of prebuilt 3rd party libraries for use with VPL.

- Unpack content of this archive into the 'VPL/3rdParty' source directory
  or somewhere else...
  
- Enable the VPL_PREFER_SUPPLIED_3RDPARTY_LIBS option when running the CMake
  utility, and modify the variable VPL_3RDPARTY_DIR if necessary.
